Требования
---
   - Centos 7

Установка необходимого ПО
---
1. Устанавливаем Percona yum repository
	    
	    sudo yum install https://repo.percona.com/yum/percona-release-latest.noarch.rpm
	    
2. Enable the repository

        sudo percona-release enable-only tools release

    If Percona XtraBackup is intented to be used in combination with the upstream MySQL Server, you only need to enable the tools repository
        
        percona-release enable-only tools
        
3. Устанавливаем необходимые библиотеки

        sudo yum install qpress libev
        
4. Устанавливаем percona xtrabackup

    для mysql 5.7 based
    
        sudo yum install percona-xtrabackup-24

Подготовка
---
1. Создать в mysql пользователя backup с необходимыми правами
    
        mysql -u root -p
    
    mysql запросит пароль пользователя root, необходимо его ввести.
    
    Первое, что нужно сделать, это создать отдельного пользователя MySQL для работы с задачами резервного копирования. Предоставьте этому пользователю только те привилегии, которые необходимы для безопасного копирования данных во время работы системы.
    
    Выберите сложный пароль вместо password
        
        CREATE USER 'backup'@'localhost' IDENTIFIED BY 'password';
        
    Нужно предоставить новому пользователю права, необходимые для выполнения всех действий резервного копирования в системе базы данных. Предоставьте необходимые привилегии и примените их к текущему сеансу:
 
        GRANT RELOAD, LOCK TABLES, REPLICATION CLIENT, CREATE TABLESPACE, PROCESS, SUPER, CREATE, INSERT, SELECT ON *.* TO 'backup'@'localhost';
        FLUSH PRIVILEGES;
        	
    Получим физический путь к данным mysql, выполнив еще один запрос
    
        SELECT @@datadir;
        
        +-----------------+
        | @@datadir       |
        +-----------------+
        | /var/lib/mysql/ |
        +-----------------+
        1 row in set (0.00 sec)
        
    Запомним адрес, он нам еще понадобится. Теперь можно закрыть оболочку MySQL
        
        exit
    
2. Создать системного пользователя backup с необходимыми правами

    создадим пользователя
    
        sudo useradd backup
        
    задим пользователю пароль
    
        passwd backup
        
    В результате будет создана следующая запись в файле /etc/passwd:
    
        backup:х:535:20: :/home/backup:/bin/sh
        
    добавим пользователя backup в группу backup
    
        sudo usermod -aG backup backup
        
    добавим пользователя backup в группу mysql
    
        sudo usermod -aG mysql backup
        
    добавим пользователя root в группу backup
    
        sudo usermod -aG backup ${USER}
        
    проверим состав нашей группы
    
        grep backup /etc/group
        
        /etc/group:mysql:x:27:backup
        /etc/group:backup:x:1004:backup,root
        
    Новая группа недоступна в текущем сеансе автоматически. Чтобы обновить список групп, доступных пользователю root, выйдите из системы и войдите снова, либо введите:
    
        exec su - ${USER}
    
    При этом будет запрошен пароль пользователя root. Убедитесь, что в текущем сеансе у вас есть доступ к группе backup:
    
        id -nG
        8host root backup
    
    Теперь пользователь root сможет пользоваться данными группы backup.
        
3. Создать необходимые папки и назначить на них права

    Далее нужно сделать каталог /var/lib/mysql и его подкаталоги доступными для группы mysql, предоставив права на изменение. В противном случае пользователь backup не сможет войти в эти каталоги, хотя он является членом группы mysql.
    
    Примечание: Если значение datadir – не /var/lib/mysql, укажите правильный путь к каталогу.
    
    Чтобы дать группе mysql доступ к каталогам данных MySQL, введите:
    
        sudo find /var/lib/mysql -type d -exec chmod 775 {} \;
    
4. Создать конфигурационный файл для бэкапов

    Создадим конфигурационный файл, который будут использовать наши скрипты в папке для бэкапов
    
        sudo vim my_backup.cnf
                    
    **my_backup.cnf**
                    
        [client]
        user=backup
        password=password
        host=localhost


Создание бэкапа
---
для создания бэкапа необходимо выполнить команду "mysql-backup" от пользователя backup

        su backup -c "mysql-backup"
        
все бэкапы будут доступны в папке /home/backup/MM-DD где
            
- MM - текущий месяц
- DD - текущий день 

Развернуть бэкап
---
	
1. Распаковать бэкап

    su backup -c "mysql-extract"
    
2. Подготовить бэкап

    su backup -c "mysql-prepare"

2. Востановить БД

Backup looks to be fully prepared.  Please check the "prepare-progress.log" file
to verify before continuing.

If everything looks correct, you can apply the restored files.

First, stop MySQL and move or remove the contents of the MySQL data directory:
    
        sudo systemctl stop mysql
        sudo mv /var/lib/mysql/ /tmp/
    
Then, recreate the data directory and  copy the backup files:
    
        sudo mkdir /var/lib/mysql
        sudo xtrabackup --copy-back --target-dir=/var/www/backup/restore/full-20-120829
    
Afterward the files are copied, adjust the permissions and restart the service:
    
        sudo chown -R mysql:mysql /var/lib/mysql
        sudo find /var/lib/mysql -type d -exec chmod 750 {} \;
        sudo systemctl start mysql

Источники
---
- https://www.8host.com/blog/nastrojka-rezervnogo-kopirovaniya-mysql-s-pomoshhyu-percona-xtrabackup-v-ubuntu-16-04/