#!/bin/bash

export LC_ALL=C

rm -rf /var/www/backup && cp -r /var/www/nfs/catalog.toledo24.ru/$(date +%V) /var/www/backup 
chown -R backup:backup /var/www/backup
su backup -c "cd /var/www/backup && mysql-extract *.xbstream"
su backup -c "cd /var/www/backup/restore && mysql-prepare"
systemctl stop mysql
rm -rf /tmp/mysql
mv /var/lib/mysql/ /tmp
mkdir /var/lib/mysql
directory_name=$(ls /var/www/backup/restore/ | grep full)
xtrabackup --copy-back --target-dir=/var/www/backup/restore/$directory_name
chown -R mysql:mysql /var/lib/mysql
find /var/lib/mysql -type d -exec chmod 750 {} \;
cd /var/lib
tar -cvf backup.tar.gz mysql
systemctl start mysql
rm /var/www/its2ru/local/.env/backup.tar.gz
mv backup.tar.gz /var/www/its2ru/local/.env/
systemctl status mysql